﻿namespace ChatKlient
{
	partial class fRegister
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fRegister));
			this.lLogin = new System.Windows.Forms.Label();
			this.tbRegisterUsername = new System.Windows.Forms.TextBox();
			this.bRegister = new System.Windows.Forms.Button();
			this.tbRegisterEmail = new System.Windows.Forms.TextBox();
			this.tbRegisterPassword = new System.Windows.Forms.TextBox();
			this.tbRegisterPasswordConfirm = new System.Windows.Forms.TextBox();
			this.lUserName = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lLogin
			// 
			this.lLogin.AutoSize = true;
			this.lLogin.Cursor = System.Windows.Forms.Cursors.Hand;
			this.lLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.lLogin.Location = new System.Drawing.Point(161, 298);
			this.lLogin.Name = "lLogin";
			this.lLogin.Size = new System.Drawing.Size(128, 13);
			this.lLogin.TabIndex = 7;
			this.lLogin.Text = "Have an account? Log in";
			this.lLogin.Click += new System.EventHandler(this.lLogin_Click);
			// 
			// tbRegisterUsername
			// 
			this.tbRegisterUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.tbRegisterUsername.Location = new System.Drawing.Point(95, 30);
			this.tbRegisterUsername.MaxLength = 20;
			this.tbRegisterUsername.Name = "tbRegisterUsername";
			this.tbRegisterUsername.Size = new System.Drawing.Size(260, 29);
			this.tbRegisterUsername.TabIndex = 6;
			this.tbRegisterUsername.Text = "user";
			this.tbRegisterUsername.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.tbRegisterUsername.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbRegisterUsername_KeyDown);
			// 
			// bRegister
			// 
			this.bRegister.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.bRegister.Location = new System.Drawing.Point(164, 314);
			this.bRegister.Name = "bRegister";
			this.bRegister.Size = new System.Drawing.Size(123, 35);
			this.bRegister.TabIndex = 5;
			this.bRegister.Text = "Register";
			this.bRegister.UseVisualStyleBackColor = true;
			this.bRegister.Click += new System.EventHandler(this.bRegister_Click);
			// 
			// tbRegisterEmail
			// 
			this.tbRegisterEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.tbRegisterEmail.Location = new System.Drawing.Point(95, 95);
			this.tbRegisterEmail.Name = "tbRegisterEmail";
			this.tbRegisterEmail.Size = new System.Drawing.Size(260, 29);
			this.tbRegisterEmail.TabIndex = 8;
			this.tbRegisterEmail.Text = "email@example.com";
			this.tbRegisterEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// tbRegisterPassword
			// 
			this.tbRegisterPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.tbRegisterPassword.Location = new System.Drawing.Point(95, 164);
			this.tbRegisterPassword.Name = "tbRegisterPassword";
			this.tbRegisterPassword.Size = new System.Drawing.Size(260, 29);
			this.tbRegisterPassword.TabIndex = 9;
			this.tbRegisterPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.tbRegisterPassword.UseSystemPasswordChar = true;
			// 
			// tbRegisterPasswordConfirm
			// 
			this.tbRegisterPasswordConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.tbRegisterPasswordConfirm.Location = new System.Drawing.Point(95, 226);
			this.tbRegisterPasswordConfirm.Name = "tbRegisterPasswordConfirm";
			this.tbRegisterPasswordConfirm.Size = new System.Drawing.Size(260, 29);
			this.tbRegisterPasswordConfirm.TabIndex = 10;
			this.tbRegisterPasswordConfirm.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.tbRegisterPasswordConfirm.UseSystemPasswordChar = true;
			this.tbRegisterPasswordConfirm.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbRegisterPasswordConfirm_KeyUp);
			// 
			// lUserName
			// 
			this.lUserName.AutoSize = true;
			this.lUserName.Location = new System.Drawing.Point(92, 14);
			this.lUserName.Name = "lUserName";
			this.lUserName.Size = new System.Drawing.Size(58, 13);
			this.lUserName.TabIndex = 11;
			this.lUserName.Text = "Username:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(92, 79);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(35, 13);
			this.label2.TabIndex = 12;
			this.label2.Text = "Email:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(92, 148);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(56, 13);
			this.label3.TabIndex = 13;
			this.label3.Text = "Password:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(92, 210);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(93, 13);
			this.label4.TabIndex = 14;
			this.label4.Text = "Confirm password:";
			// 
			// fRegister
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(434, 361);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.lUserName);
			this.Controls.Add(this.tbRegisterPasswordConfirm);
			this.Controls.Add(this.tbRegisterPassword);
			this.Controls.Add(this.tbRegisterEmail);
			this.Controls.Add(this.lLogin);
			this.Controls.Add(this.tbRegisterUsername);
			this.Controls.Add(this.bRegister);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "fRegister";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "fRegister";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.fRegister_FormClosed);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lLogin;
		private System.Windows.Forms.TextBox tbRegisterUsername;
		private System.Windows.Forms.Button bRegister;
		private System.Windows.Forms.TextBox tbRegisterEmail;
		private System.Windows.Forms.TextBox tbRegisterPassword;
		private System.Windows.Forms.TextBox tbRegisterPasswordConfirm;
		private System.Windows.Forms.Label lUserName;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
	}
}