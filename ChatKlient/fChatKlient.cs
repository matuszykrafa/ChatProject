﻿using BibliotekaKlas;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatKlient
{
	public partial class fChatKlient : Form
	{
		public fChatKlient(string hostN, int portN)
		{
				InitializeComponent();
			
			//Jestem Grzegorz
			host = hostN;
			port = portN;
			//nick = nickN;
			this.Text = nick;

			lbClientList.MouseDoubleClick += new MouseEventHandler(lbClientList_DoubleClick);
			wbChat.DocumentText = wbInit;

			repairBrowser();

			bReconnect.Enabled = false;
			lStatus.Text = "Status: łączenie...";
			//clientThread = new Thread(clientWork);
			//clientThread.Start();
		}
		#region Deklaracja zmiennych
		public string host;
		public int port;
		public string nick;
		private string wbInit = "<html><head><style>* { font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; } .message-box-left { width: 100%; display: flex; justify-content: flex-start}.message-box-right { width: 100%; display: flex; justify-content: flex-end} .message-received { width:70%; margin: 10px; padding: 10px; background-color: lightgray; border-radius: 10px; border-bottom-left-radius: 0; color: #000408; } .message-sent { width:70%; margin: 10px; padding: 10px; background-color: dodgerblue; border-radius: 10px; border-bottom-right-radius: 0; color: white; } .nick { padding: 5px; font-size: 12px; } .message { margin: 0; padding: 5px; padding-left: 10px; font-size: 16px; line-height: 1.5; }</style></head><body>";

		public TcpClient client = null;
		public StreamReader reading = null;
		public StreamWriter writing = null;
		private List<string> ClientList = new List<string>();
		private string Odbiorca = null;

		private bool activeClient = true;
		//Thread clientThread;
		#endregion

		#region Naprawa przegladarki
		private void repairBrowser()
		{
			int BrowserVer, RegVal;

			// get the installed IE version
			using (WebBrowser Wb = new WebBrowser())
				BrowserVer = Wb.Version.Major;

			// set the appropriate IE version
			if (BrowserVer >= 11)
				RegVal = 11001;
			else if (BrowserVer == 10)
				RegVal = 10001;
			else if (BrowserVer == 9)
				RegVal = 9999;
			else if (BrowserVer == 8)
				RegVal = 8888;
			else
				RegVal = 7000;

			// set the actual key
			using (RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION", RegistryKeyPermissionCheck.ReadWriteSubTree))
				if (Key.GetValue(System.Diagnostics.Process.GetCurrentProcess().ProcessName + ".exe") == null)
					Key.SetValue(System.Diagnostics.Process.GetCurrentProcess().ProcessName + ".exe", RegVal, RegistryValueKind.DWord);
		}
		#endregion

		#region Wątek
		private void clientWorkNewThread()
		{
			try {
				client = new TcpClient(fConnect.host, fConnect.port);
				reading = new StreamReader(client.GetStream());
				writing = new StreamWriter(client.GetStream());
				send("Inicjalizacja", "Serwer", "init");
				clientWork();

			}
			catch {
				lStatus.Invoke(new MethodInvoker(delegate { lStatus.Text = "Nie można połączyć się z " + fConnect.host + " na porcie: " + fConnect.port; }));
				bReconnect.Invoke(new MethodInvoker(delegate { bReconnect.Enabled = true; }));
			}
		}
		#endregion

		#region Połączenie
		public void clientWork()
		{
			try {
				
				lStatus.Invoke(new MethodInvoker(delegate { lStatus.Text = "Status: nawiązano połączenie z " + fConnect.host + " na porcie: " + fConnect.port; }));
				//send("Inicjalizacja", "Serwer", "init");
				//this.Invoke(new MethodInvoker(delegate { this.Text = nick; this.Show(); }));

				while (true) {
					try {
						
						string messageRecived = reading.ReadLine();
						var receivedJson = JsonConvert.DeserializeObject<JsonMessage>(messageRecived);
						if (receivedJson.wiadomosc_typ == "server") {
							
							if(receivedJson.wiadomosc_dane == "logged") {
								MessageBox.Show("Jesteś już zalogowany!", "Logged");
								Application.Restart();
							}
							else if(receivedJson.wiadomosc_dane == "banned") {
								MessageBox.Show("Jesteś zbanowany!", "Banned");
								Application.Restart();
							}
							else if(receivedJson.wiadomosc_dane == "registered") {
								
								//Console.WriteLine("registered");
								//MessageBox.Show("Pomyślnie zarejestrowano!", "Success");
							}
							else {
								this.Invoke(new MethodInvoker(delegate { this.Show(); }));
							}
						}
						else if (receivedJson.wiadomosc_typ == "clientList") {
							ClientList = receivedJson.wiadomosc_dane.Split(';').ToList();
							refreshList();
						}
						else if (receivedJson.wiadomosc_typ == "text") {

							if (receivedJson.wiadomosc_dane != null) {
								if (Odbiorca == receivedJson.nadawca) {
									Console.WriteLine(receivedJson.wiadomosc_dane);
									//WTF OPOZNIENIE ????
									refreshList();
                                    string wiadomoscConvert = emoji(receivedJson.wiadomosc_dane);
                                    wbChat.Invoke(new MethodInvoker(delegate {
										wbChat.DocumentText += "<div class='message-box-left'><div class='message-received'> <span class='nick'>" + receivedJson.czas_wyslania + " " + receivedJson.nadawca + "</span> <p class='message'>" + wiadomoscConvert + "</p></div></div><script>document.body.scrollTop = document.body.scrollHeight</script>";
									}));
								}
								else if (receivedJson.nadawca == "Server") {
									refreshList();
									wbChat.Invoke(new MethodInvoker(delegate {
										wbChat.DocumentText += "<div class='message-box-left'><div class='message-received'> <span class='nick'>" + receivedJson.czas_wyslania + " <i style='color:red'>" + receivedJson.nadawca + "</i></span> <p class='message'>" + receivedJson.wiadomosc_dane + "</p></div></div><script>document.body.scrollTop = document.body.scrollHeight</script>";
									}));
								}
								else {

								}
							}
						}
						else if (receivedJson.wiadomosc_typ == "textZwrotSent") {
							if (receivedJson.wiadomosc_dane != null) {
								refreshList();
                                string wiadomoscConvert = emoji(receivedJson.wiadomosc_dane);
                                wbChat.Invoke(new MethodInvoker(delegate {
									wbChat.DocumentText += "<div class='message-box-right'><div class='message-sent'> <span class='nick'>" + receivedJson.czas_wyslania + "</span> <p class='message'>" + wiadomoscConvert + "</p></div></div>"; //<script>document.body.scrollTop = document.body.scrollHeight</script>
								}));
							}
						}
						else if (receivedJson.wiadomosc_typ == "textZwrotReceived") {
							refreshList();
                            string wiadomoscConvert = emoji(receivedJson.wiadomosc_dane);
                            wbChat.Invoke(new MethodInvoker(delegate {
								wbChat.DocumentText += "<div class='message-box-left'><div class='message-received'> <span class='nick'>" + receivedJson.czas_wyslania + " " + receivedJson.nadawca + "</span> <p class='message'>" + wiadomoscConvert + "</p></div></div>"; //<script>document.body.scrollTop = document.body.scrollHeight</script>
							}));
						}
						else if(receivedJson.wiadomosc_typ == "textZwrotKoniec") {
							refreshList();
							wbChat.Invoke(new MethodInvoker(delegate { wbChat.Document.Body.ScrollIntoView(false); }));
						}
						else {
							//MessageBox.Show("b1");
							break;
						}

					}
					catch {
						//MessageBox.Show("b2");
						break;
					}
				}

				client.Close();
				writing.Close();
				reading.Close();
				if (activeClient) {
					
					lStatus.Invoke(new MethodInvoker(delegate { lStatus.Text = "Status: przerwano połączenie z serwerem"; lStatus.Refresh(); }));
					bReconnect.Invoke(new MethodInvoker(delegate { bReconnect.Enabled = true; }));
				}

				//tbNadawca.Invoke((MethodInvoker)delegate { tbNadawca.Enabled = true; });
				

			}
			catch (FormatException) {
				lStatus.Invoke(new MethodInvoker(delegate { lStatus.Text = "Status: błąd - Nie udało się nawiązać połączenia! Zły adres IP"; }));
				bReconnect.Invoke(new MethodInvoker(delegate { bReconnect.Enabled = true; }));
				//MessageBox.Show(ex.ToString());
			}
			catch (SocketException) {
				if (activeClient) {
					
					lStatus.Invoke(new MethodInvoker(delegate { lStatus.Text = "Status: błąd - Nie udało się nawiązać połączenia!"; }));
					bReconnect.Invoke(new MethodInvoker(delegate { bReconnect.Enabled = true; }));
				}
				//tbNadawca.Invoke((MethodInvoker)delegate { tbNadawca.Enabled = true; });
			}
		}
		#endregion

		#region Lista klientów
		private void refreshList()
		{
			lbClientList.Invoke((MethodInvoker)delegate { lbClientList.Items.Clear(); });
			foreach (string ClientName in ClientList) {
				if (ClientName != nick)
					lbClientList.Invoke((MethodInvoker)delegate { lbClientList.Items.Add(ClientName); });
			}
		}
		#endregion

		#region Wysyłanie wiadomości (enter, send, sendPrepare)
		private void rtbMessage_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter) {
				e.Handled = true;
				send(rtbMessage.Text, Odbiorca, "text");
			}
		}
		private string sendPrepare(string msg, string adresat, string typ)
		{
			JsonMessage wiadomoscObiekt = new JsonMessage();
			wiadomoscObiekt.nadawca = nick;
			wiadomoscObiekt.adresat = adresat;
			wiadomoscObiekt.czas_wyslania = CurrentTimeSend();
			wiadomoscObiekt.wiadomosc_dane = msg;
			wiadomoscObiekt.wiadomosc_typ = typ;
			string output = JsonConvert.SerializeObject(wiadomoscObiekt);
			return output;
		}
		public void send(string msg, string adresat, string typ)
		{

            if (writing != null && writing.BaseStream != null)
            {

                writing.WriteLine(sendPrepare(msg, adresat, typ));

                writing.Flush();
                if (msg != "Inicjalizacja" && typ != "setReceiver" && typ != "register" && typ != "login" && typ != "logout")
                {
                    string wiadomoscConvert = emoji(rtbMessage.Text);
                    
                    wbChat.Invoke(new MethodInvoker(delegate
                    {
                        wbChat.DocumentText += "<div class='message-box-right'><div class='message-sent'> <span class='nick'>" + CurrentTimeSend() + "</span> <p class='message'>" + wiadomoscConvert + "</p></div></div><script>document.body.scrollTop = document.body.scrollHeight</script>";
                    }));
                }
            }
			rtbMessage.Invoke(new MethodInvoker(delegate { rtbMessage.Clear(); }));
		}
        private string emoji(string msg)
        {
            string wiadomoscConvert = msg;
            wiadomoscConvert = wiadomoscConvert.Replace(":)", "&#128578;");
            wiadomoscConvert = wiadomoscConvert.Replace("<3", "&#10084;");
            wiadomoscConvert = wiadomoscConvert.Replace(":D", "&#128515;");
            wiadomoscConvert = wiadomoscConvert.Replace(":*", "&#128536;");
            wiadomoscConvert = wiadomoscConvert.Replace(";(", "&#128549;");
            wiadomoscConvert = wiadomoscConvert.Replace(":(", "&#128577;");
            return wiadomoscConvert;
        }

		public string CurrentTimeSend()
		{
			string time = "";
			int ihour = DateTime.Now.Hour;
			string hour;
			if (ihour < 10)
				hour = "0" + ihour.ToString();
			else
				hour = ihour.ToString();

			int imin = DateTime.Now.Minute;
			string min;
			if (imin < 10)
				min = "0" + imin.ToString();
			else
				min = imin.ToString();
			time = hour + ":" + min;
			return time;
		}
		#endregion

		#region ClientList
		private void lbClientList_DoubleClick(object sender, MouseEventArgs e)

		{
			try {
				int index = this.lbClientList.IndexFromPoint(e.Location);
				if (index != System.Windows.Forms.ListBox.NoMatches) {
					// MessageBox.Show(lbClientList.SelectedItem.ToString());
					Odbiorca = lbClientList.SelectedItem.ToString();
					this.Text = nick + ": " + Odbiorca;
					wbChat.Invoke(new MethodInvoker(delegate {
						wbChat.DocumentText = "";
						wbChat.DocumentText = wbInit;
					}));
					send(Odbiorca, "Server", "setReceiver");
				}
			}
			catch {

			}
		}
		#endregion

		#region Reconnect, wyłączenie
		private void bReconnect_Click(object sender, EventArgs e)
		{
			bReconnect.Invoke(new MethodInvoker(delegate { bReconnect.Enabled = false; }));
			lStatus.Invoke(new MethodInvoker(delegate { lStatus.Text = "Status: łączenie..."; }));

			Thread clientThread = new Thread(clientWorkNewThread);
			clientThread.Start();
		}

		private void Stop()
		{
			if (client != null)
				client.Close();

		}

		private void ClientTcp_FormClosing(object sender, FormClosingEventArgs e)
		{
			Stop();
			activeClient = false;
			Application.Exit();
		}
		#endregion


        private void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void wyjdzToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
