﻿using BibliotekaKlas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace ChatKlient
{
	public partial class fRegister : Form
	{
		
		public fRegister()
		{
			InitializeComponent();
		}

		private void lLogin_Click(object sender, EventArgs e)
		{
			this.Invoke(new MethodInvoker(delegate { this.Hide(); }));
			fLogin.formLogin.Invoke(new MethodInvoker(delegate { fLogin.formLogin.Show(); }));
		}

		private void fRegister_FormClosed(object sender, FormClosedEventArgs e)
		{
			this.Invoke(new MethodInvoker(delegate { this.Hide(); }));
			fLogin.formLogin.Invoke(new MethodInvoker(delegate { fLogin.formLogin.Show(); }));
		}

		private void bRegister_Click(object sender, EventArgs e)
		{

			string username = tbRegisterUsername.Text;
			string email = tbRegisterEmail.Text;
			string password = tbRegisterPassword.Text;
			
			if(password == tbRegisterPasswordConfirm.Text) {

				JsonClient clientDane = new JsonClient();
				clientDane.user = username;
				clientDane.email = email;
				clientDane.password = password;
				clientDane.salt = "salt";
				string clientDaneString = JsonConvert.SerializeObject(clientDane);
				Console.WriteLine("REGISTER");
				fConnect.clientForm.send(clientDaneString, "Serwer", "register");
				while (true) {
					try {

						string messageRecived = fConnect.reader.ReadLine();
						var receivedJson = JsonConvert.DeserializeObject<JsonMessage>(messageRecived);
						if (receivedJson.wiadomosc_typ == "server") {
							if (receivedJson.wiadomosc_dane == "allowRegister") {
								MessageBox.Show("Registration completed!", "Success");
								fRegister.ActiveForm.Close();
								break;
							}
							else if(receivedJson.wiadomosc_dane == "denyRegister") {
								MessageBox.Show("Registration failed!", "Failed");
								break;
							}
							else {
								break;
							}
						}
					}
					catch {
						break;
					}
				}
				
				//MessageBox.Show("Success");

			}
			else {
				tbRegisterPasswordConfirm.BackColor = Color.Red;
			}
		}

		private void tbRegisterPasswordConfirm_KeyUp(object sender, KeyEventArgs e)
		{
			if (tbRegisterEmail.Text == tbRegisterPasswordConfirm.Text) {
				tbRegisterPasswordConfirm.BackColor = Color.White;
			}
		}

		private void tbRegisterUsername_KeyDown(object sender, KeyEventArgs e)
		{
			e.SuppressKeyPress = (e.KeyCode == Keys.Space || e.KeyCode == Keys.OemSemicolon);
		}
	}
}
