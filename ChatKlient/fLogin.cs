﻿using BibliotekaKlas;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ChatKlient
{
	public partial class fLogin : Form
	{
		public static Form formLogin;
		public fLogin()
		{
			InitializeComponent();
			formLogin = this;
		}



		private void bLogin_Click(object sender, EventArgs e)
		{
            login();
		}

		private void lCreateAccount_Click(object sender, EventArgs e)
		{
			this.Invoke(new MethodInvoker(delegate { this.Hide(); }));
			Form formRegister = new fRegister();
			formRegister.ShowDialog();
		}

		private void fLogin_FormClosed(object sender, FormClosedEventArgs e)
		{
			Application.Exit();
		}

		private void tbLogin_KeyDown(object sender, KeyEventArgs e)
		{
			e.SuppressKeyPress = (e.KeyCode == Keys.Space || e.KeyCode == Keys.OemSemicolon);
            if (e.KeyCode == Keys.Enter)
            {
                login();
            }
        }

        private void fLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                login();
            }
        }

        private void login()
        {
            try
            {
                string nick = tbLogin.Text;
                if (nick.ToLower() == "server")
                {
                    MessageBox.Show("Nieprawidłowa nazwa");
                    tbLogin.Text = "user";
                }
                else
                {
                    JsonClient client = new JsonClient();
                    client.user = nick;
                    client.password = tbPasswordLogin.Text;

                    string clientString = JsonConvert.SerializeObject(client);
                    fConnect.clientForm.send(clientString, "Server", "login");
                    while (true)
                    {
                        try
                        {
                            string messageRecived = fConnect.reader.ReadLine();
                            var receivedJson = JsonConvert.DeserializeObject<JsonMessage>(messageRecived);
                            if (receivedJson.wiadomosc_typ == "server")
                            {
                                if (receivedJson.wiadomosc_dane == "allowLogin")
                                {
                                    //fRegister.ActiveForm.Hide();
                                    Hide();
                                    fConnect.clientForm.Invoke(new MethodInvoker(delegate {
                                        fConnect.clientForm.nick = nick;

                                        Thread clientThread = new Thread(fConnect.clientForm.clientWork);
                                        //clientThread.SetApartmentState(ApartmentState.STA);
                                        clientThread.Start();
                                        fConnect.clientForm.Text = nick;
                                        fConnect.clientForm.ShowDialog();

                                        //fConnect.clientForm.clientWork();
                                    }));

                                    break;
                                }
                                else if (receivedJson.wiadomosc_dane == "denyLogin")
                                {
                                    MessageBox.Show("Niepoprawny login lub hasło!", "Błąd");
                                    break;
                                }
                                else if (receivedJson.wiadomosc_dane == "denyLoginLogged")
                                {
                                    MessageBox.Show("Użytkownik jest już zalogowany!", "Błąd");
                                    break;
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                        catch
                        {
                            break;
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("Brak połączenia z serwerem!", "Błąd");
                Application.Restart();
            }
        }
    }
}
