﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatKlient
{
	public partial class fConnect : Form
	{
		public fConnect()
		{
			InitializeComponent();
			
			Thread connectWork = new Thread(Connect);
			connectWork.SetApartmentState(ApartmentState.STA);
			connectWork.Start();
		}
		
		public static string host = "localhost";
		public static int port = 1234;

		 

		public static StreamWriter writer;
		public static StreamReader reader;
		public static TcpClient client;

		public static fChatKlient clientForm = new fChatKlient(host, port);

		public void Connect()
		{
			try {
				client = new TcpClient(host, port);
				reader = new StreamReader(client.GetStream());
				writer = new StreamWriter(client.GetStream());

				clientForm.client = client;
				clientForm.reading = reader;
				clientForm.writing = writer;

				this.Invoke(new MethodInvoker(delegate { Hide(); }));
				Form loginForm = new fLogin();
				loginForm.ShowDialog();
				
			}
			catch {
				this.Invoke(new MethodInvoker(delegate { Hide(); }));
				Form connectOther = new fConnectOther();
				connectOther.ShowDialog();

			}
		}

	}
}
