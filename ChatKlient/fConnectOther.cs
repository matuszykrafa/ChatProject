﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatKlient
{
	public partial class fConnectOther : Form
	{
		public fConnectOther()
		{
			InitializeComponent();
		}
        private void polaczenie()
        {
            try
            {
                Console.WriteLine(fConnect.host);
                fConnect.host = tbAddress.Text;

                fConnect.port = (int)nudPort.Value;
                fConnect.client = new TcpClient(fConnect.host, fConnect.port);
                fConnect.reader = new StreamReader(fConnect.client.GetStream());
                fConnect.writer = new StreamWriter(fConnect.client.GetStream());

                fConnect.clientForm.client = fConnect.client;
                fConnect.clientForm.reading = fConnect.reader;
                fConnect.clientForm.writing = fConnect.writer;
                Form loginForm = new fLogin();

                this.Invoke(new MethodInvoker(delegate { Hide(); }));
                loginForm.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Nie udało się nawiązać połączenia", "Błąd");
                //MessageBox.Show("Nie udało połączyć się z serwerem");
                this.Show();

            }
        }
		private void bConnect_Click(object sender, EventArgs e)
		{
            polaczenie();
		}

		private void fConnectOther_FormClosed(object sender, FormClosedEventArgs e)
		{
			Application.Exit();
		}

        private void fConnectOther_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Visible)
            {
                Console.WriteLine("lgowanie");
                polaczenie();
               
            }
        }
    }
}
