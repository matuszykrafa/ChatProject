﻿namespace ChatKlient
{
    partial class fChatKlient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fChatKlient));
            this.rtbMessage = new System.Windows.Forms.RichTextBox();
            this.wbChat = new System.Windows.Forms.WebBrowser();
            this.lbClientList = new System.Windows.Forms.ListBox();
            this.bReconnect = new System.Windows.Forms.Button();
            this.lClientList = new System.Windows.Forms.Label();
            this.lStatus = new System.Windows.Forms.Label();
            this.tsConnection = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tsbPołączenie = new System.Windows.Forms.ToolStripDropDownButton();
            this.restartToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wyjdzToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsConnection.SuspendLayout();
            this.SuspendLayout();
            // 
            // rtbMessage
            // 
            this.rtbMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rtbMessage.Location = new System.Drawing.Point(29, 418);
            this.rtbMessage.Name = "rtbMessage";
            this.rtbMessage.Size = new System.Drawing.Size(442, 61);
            this.rtbMessage.TabIndex = 0;
            this.rtbMessage.Text = "";
            this.rtbMessage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rtbMessage_KeyDown);
            // 
            // wbChat
            // 
            this.wbChat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.wbChat.Location = new System.Drawing.Point(29, 45);
            this.wbChat.Margin = new System.Windows.Forms.Padding(20);
            this.wbChat.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbChat.Name = "wbChat";
            this.wbChat.Size = new System.Drawing.Size(442, 350);
            this.wbChat.TabIndex = 1;
            // 
            // lbClientList
            // 
            this.lbClientList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbClientList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbClientList.FormattingEnabled = true;
            this.lbClientList.ItemHeight = 20;
            this.lbClientList.Location = new System.Drawing.Point(494, 71);
            this.lbClientList.Margin = new System.Windows.Forms.Padding(20);
            this.lbClientList.Name = "lbClientList";
            this.lbClientList.Size = new System.Drawing.Size(121, 404);
            this.lbClientList.TabIndex = 2;
            // 
            // bReconnect
            // 
            this.bReconnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bReconnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bReconnect.Location = new System.Drawing.Point(494, 493);
            this.bReconnect.Name = "bReconnect";
            this.bReconnect.Size = new System.Drawing.Size(121, 26);
            this.bReconnect.TabIndex = 3;
            this.bReconnect.Text = "Reconnect";
            this.bReconnect.UseVisualStyleBackColor = true;
            this.bReconnect.Click += new System.EventHandler(this.bReconnect_Click);
            // 
            // lClientList
            // 
            this.lClientList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lClientList.AutoSize = true;
            this.lClientList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lClientList.Location = new System.Drawing.Point(490, 50);
            this.lClientList.Name = "lClientList";
            this.lClientList.Size = new System.Drawing.Size(125, 20);
            this.lClientList.TabIndex = 4;
            this.lClientList.Text = "Aktualnie online:";
            // 
            // lStatus
            // 
            this.lStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lStatus.AutoSize = true;
            this.lStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lStatus.Location = new System.Drawing.Point(26, 498);
            this.lStatus.Name = "lStatus";
            this.lStatus.Size = new System.Drawing.Size(48, 16);
            this.lStatus.TabIndex = 5;
            this.lStatus.Text = "Status:";
            // 
            // tsConnection
            // 
            this.tsConnection.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.tsbPołączenie});
            this.tsConnection.Location = new System.Drawing.Point(0, 0);
            this.tsConnection.Name = "tsConnection";
            this.tsConnection.Size = new System.Drawing.Size(644, 25);
            this.tsConnection.TabIndex = 6;
            this.tsConnection.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(0, 22);
            this.toolStripLabel1.Text = "toolStripLabel1";
            // 
            // tsbPołączenie
            // 
            this.tsbPołączenie.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbPołączenie.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.restartToolStripMenuItem,
            this.wyjdzToolStripMenuItem});
            this.tsbPołączenie.Image = ((System.Drawing.Image)(resources.GetObject("tsbPołączenie.Image")));
            this.tsbPołączenie.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPołączenie.Name = "tsbPołączenie";
            this.tsbPołączenie.Size = new System.Drawing.Size(76, 22);
            this.tsbPołączenie.Text = "Połączenie";
            // 
            // restartToolStripMenuItem
            // 
            this.restartToolStripMenuItem.Name = "restartToolStripMenuItem";
            this.restartToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.restartToolStripMenuItem.Text = "Restart";
            this.restartToolStripMenuItem.Click += new System.EventHandler(this.restartToolStripMenuItem_Click);
            // 
            // wyjdzToolStripMenuItem
            // 
            this.wyjdzToolStripMenuItem.Name = "wyjdzToolStripMenuItem";
            this.wyjdzToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.wyjdzToolStripMenuItem.Text = "Wyjdź";
            this.wyjdzToolStripMenuItem.Click += new System.EventHandler(this.wyjdzToolStripMenuItem_Click);
            // 
            // fChatKlient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(644, 531);
            this.Controls.Add(this.tsConnection);
            this.Controls.Add(this.lStatus);
            this.Controls.Add(this.lClientList);
            this.Controls.Add(this.bReconnect);
            this.Controls.Add(this.lbClientList);
            this.Controls.Add(this.wbChat);
            this.Controls.Add(this.rtbMessage);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(660, 570);
            this.Name = "fChatKlient";
            this.Text = "user";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ClientTcp_FormClosing);
            this.tsConnection.ResumeLayout(false);
            this.tsConnection.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbMessage;
        private System.Windows.Forms.WebBrowser wbChat;
        private System.Windows.Forms.ListBox lbClientList;
        private System.Windows.Forms.Button bReconnect;
        private System.Windows.Forms.Label lClientList;
        private System.Windows.Forms.Label lStatus;
		private System.Windows.Forms.ToolStrip tsConnection;
		private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripDropDownButton tsbPołączenie;
        private System.Windows.Forms.ToolStripMenuItem restartToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wyjdzToolStripMenuItem;
    }
}