﻿using BibliotekaKlas;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SQLiteDatabase
{
    public class UserDatabase
    {
		public static List<JsonClient> LoadUsers()
		{
			using (IDbConnection cnn = new SQLiteConnection(DatabaseConnection.LoadConnectrionString())) {
				var output = cnn.Query<JsonClient>("select * from Users", new DynamicParameters());
				return output.ToList();
			}

		}
		
		public static void RegisterUser(JsonClient client)
		{
			HashPassword(client);
			using (IDbConnection cnn = new SQLiteConnection(DatabaseConnection.LoadConnectrionString())) {
				cnn.Execute("insert into Users (user, email, password, salt) values ('" + client.user + "', '" + client.email + "', '" + client.password + "', '" + client.salt + "')", client);
			}
		}

		private static void HashPassword(JsonClient client)
		{
			var salt = new byte[32];
			var hash = new byte[32];
			new RNGCryptoServiceProvider().GetBytes(salt);
			hash = new Rfc2898DeriveBytes(client.password, salt, 10000).GetBytes(32);
			client.password = Convert.ToBase64String(hash);
			client.salt = Convert.ToBase64String(salt);
		}
	}
}
