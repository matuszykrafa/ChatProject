﻿using BibliotekaKlas;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;

namespace SQLiteDatabase
{
	public class BansDatabase
	{
		public static List<BanClass> LoadBan()
		{
			using (IDbConnection cnn = new SQLiteConnection(DatabaseConnection.LoadConnectrionString())) {
				var output = cnn.Query<BanClass>("select * from Bans", new DynamicParameters());

				return output.ToList();
			}
		}

		public static void SaveBan(BanClass ban)
		{
			using (IDbConnection cnn = new SQLiteConnection(DatabaseConnection.LoadConnectrionString())) {
				cnn.Execute("insert into Bans (user, ip) values ('" + ban.user + "', '" + ban.ip + "')", ban);
			}
		}
	}
}
