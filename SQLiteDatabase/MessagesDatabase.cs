﻿using BibliotekaKlas;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;

namespace SQLiteDatabase
{
	public class MessagesDatabase
	{
		public static List<JsonMessage> LoadMessage()
		{
			using (IDbConnection cnn = new SQLiteConnection(DatabaseConnection.LoadConnectrionString())) {
				var output = cnn.Query<JsonMessage>("select * from Messages", new DynamicParameters());
				return output.ToList();
			}
			
		}

		public static void SaveMessage(JsonMessage message)
		{
			using (IDbConnection cnn = new SQLiteConnection(DatabaseConnection.LoadConnectrionString())) {
				cnn.Execute("insert into Messages (nadawca, adresat, czas_wyslania, wiadomosc_dane, wiadomosc_typ) values ('" + message.nadawca + "', '" + message.adresat + "', '" + message.czas_wyslania + "', '" + message.wiadomosc_dane + "', '" + message.wiadomosc_typ + "')", message);
			}
		}
	}
}
