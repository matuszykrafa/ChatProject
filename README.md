# ChatProject

Projekt serwera oraz klienta chatu. 
Serwer posiada szyfrowane logowanie, rejestrację oraz historię czatu zapisywane w bazie MySQLi.
Klient posiada możliwość wybierania odbiorcy, przesyłania danych.


Serwer:
 - run ChatSerwer/bin/Debug/ChatSerwer.exe

![Screenshot](screens/2020-07-04 08_51_38-Serwer.png)

Klient:
 - run ChatKlient/bin/Debug/ChatKlient.exe

![Screenshot](screens/2020-07-04 08_39_24-Login.png)
![Screenshot](screens/2020-07-04 08_39_07-fRegister.png)
![Screenshot](screens/2020-07-04 08_44_11-test1_ test12.png)
![Screenshot](screens/2020-07-04 08_44_19-test12_ test1.png)
