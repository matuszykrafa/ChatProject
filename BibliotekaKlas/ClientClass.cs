﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace BibliotekaKlas
{
	public class ClientClass
	{
		public TcpClient client;
		public IPEndPoint IP;
		public NetworkStream ns;
		public StreamWriter writer;
		public StreamReader reader;
		public string nick;


		public void Connection(object arg)
		{
			client = (TcpClient)arg;
			IP = (IPEndPoint)client.Client.RemoteEndPoint;
			ns = client.GetStream();
			writer = new StreamWriter(ns);
			reader = new StreamReader(ns);
		}
	}
}
