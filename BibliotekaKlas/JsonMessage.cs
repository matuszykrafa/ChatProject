﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BibliotekaKlas
{
	public class JsonMessage
	{
		public string nadawca { get; set; }
		public string adresat { get; set; }
		public string czas_wyslania { get; set; }
		public string wiadomosc_typ { get; set; }
		public string wiadomosc_dane { get; set; }
	}
}
