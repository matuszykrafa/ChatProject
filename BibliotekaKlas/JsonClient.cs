﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotekaKlas
{
	public class JsonClient
	{
		public string user { get; set; }
		public string email { get; set; }
		public string password { get; set; }
		public string salt { get; set; }
	}
}
