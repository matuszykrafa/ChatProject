﻿namespace ChatSerwer
{
    partial class fSerwer
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fSerwer));
			this.bStart = new System.Windows.Forms.Button();
			this.bStop = new System.Windows.Forms.Button();
			this.lAddress = new System.Windows.Forms.Label();
			this.lPort = new System.Windows.Forms.Label();
			this.tbAddress = new System.Windows.Forms.TextBox();
			this.lbServerMessages = new System.Windows.Forms.ListBox();
			this.cmsListBox = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.wyczyśćToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.zapiszLogiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.kopiujDoSchowkaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.kopiujZaznaczenieDoSchowkaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.lbClientList = new System.Windows.Forms.ListBox();
			this.cmsClientListBox = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.kopiujZaznaczenieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.wyrzucToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.zbanujToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.rtbMessage = new System.Windows.Forms.RichTextBox();
			this.nudPort = new System.Windows.Forms.NumericUpDown();
			this.saveLogsDialog = new System.Windows.Forms.SaveFileDialog();
			this.cmsListBox.SuspendLayout();
			this.cmsClientListBox.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudPort)).BeginInit();
			this.SuspendLayout();
			// 
			// bStart
			// 
			this.bStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.bStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.bStart.Location = new System.Drawing.Point(544, 43);
			this.bStart.Margin = new System.Windows.Forms.Padding(10);
			this.bStart.Name = "bStart";
			this.bStart.Size = new System.Drawing.Size(80, 40);
			this.bStart.TabIndex = 0;
			this.bStart.Text = "Start";
			this.bStart.UseVisualStyleBackColor = true;
			this.bStart.Click += new System.EventHandler(this.bStart_Click);
			// 
			// bStop
			// 
			this.bStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.bStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.bStop.Location = new System.Drawing.Point(663, 43);
			this.bStop.Margin = new System.Windows.Forms.Padding(10);
			this.bStop.Name = "bStop";
			this.bStop.Size = new System.Drawing.Size(80, 40);
			this.bStop.TabIndex = 1;
			this.bStop.Text = "Stop";
			this.bStop.UseVisualStyleBackColor = true;
			this.bStop.Click += new System.EventHandler(this.bStop_Click);
			// 
			// lAddress
			// 
			this.lAddress.AutoSize = true;
			this.lAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.lAddress.Location = new System.Drawing.Point(24, 27);
			this.lAddress.Margin = new System.Windows.Forms.Padding(15);
			this.lAddress.Name = "lAddress";
			this.lAddress.Size = new System.Drawing.Size(51, 20);
			this.lAddress.TabIndex = 2;
			this.lAddress.Text = "Adres";
			// 
			// lPort
			// 
			this.lPort.AutoSize = true;
			this.lPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.lPort.Location = new System.Drawing.Point(24, 77);
			this.lPort.Margin = new System.Windows.Forms.Padding(15);
			this.lPort.Name = "lPort";
			this.lPort.Size = new System.Drawing.Size(38, 20);
			this.lPort.TabIndex = 3;
			this.lPort.Text = "Port";
			// 
			// tbAddress
			// 
			this.tbAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.tbAddress.Location = new System.Drawing.Point(105, 24);
			this.tbAddress.Margin = new System.Windows.Forms.Padding(15);
			this.tbAddress.Name = "tbAddress";
			this.tbAddress.Size = new System.Drawing.Size(200, 26);
			this.tbAddress.TabIndex = 4;
			this.tbAddress.Text = "0.0.0.0";
			// 
			// lbServerMessages
			// 
			this.lbServerMessages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lbServerMessages.ContextMenuStrip = this.cmsListBox;
			this.lbServerMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.lbServerMessages.FormattingEnabled = true;
			this.lbServerMessages.ItemHeight = 20;
			this.lbServerMessages.Location = new System.Drawing.Point(24, 117);
			this.lbServerMessages.Margin = new System.Windows.Forms.Padding(15, 5, 4, 15);
			this.lbServerMessages.Name = "lbServerMessages";
			this.lbServerMessages.Size = new System.Drawing.Size(513, 304);
			this.lbServerMessages.TabIndex = 6;
			// 
			// cmsListBox
			// 
			this.cmsListBox.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wyczyśćToolStripMenuItem,
            this.zapiszLogiToolStripMenuItem,
            this.kopiujDoSchowkaToolStripMenuItem,
            this.kopiujZaznaczenieDoSchowkaToolStripMenuItem});
			this.cmsListBox.Name = "cmsListBox";
			this.cmsListBox.Size = new System.Drawing.Size(240, 92);
			this.cmsListBox.Opening += new System.ComponentModel.CancelEventHandler(this.cmsListBox_Opening);
			// 
			// wyczyśćToolStripMenuItem
			// 
			this.wyczyśćToolStripMenuItem.Name = "wyczyśćToolStripMenuItem";
			this.wyczyśćToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
			this.wyczyśćToolStripMenuItem.Text = "Wyczyść";
			this.wyczyśćToolStripMenuItem.Click += new System.EventHandler(this.wyczyśćToolStripMenuItem_Click);
			// 
			// zapiszLogiToolStripMenuItem
			// 
			this.zapiszLogiToolStripMenuItem.Name = "zapiszLogiToolStripMenuItem";
			this.zapiszLogiToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
			this.zapiszLogiToolStripMenuItem.Text = "Zapisz logi";
			this.zapiszLogiToolStripMenuItem.Click += new System.EventHandler(this.zapiszLogiToolStripMenuItem_Click);
			// 
			// kopiujDoSchowkaToolStripMenuItem
			// 
			this.kopiujDoSchowkaToolStripMenuItem.Name = "kopiujDoSchowkaToolStripMenuItem";
			this.kopiujDoSchowkaToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
			this.kopiujDoSchowkaToolStripMenuItem.Text = "Kopiuj do schowka";
			this.kopiujDoSchowkaToolStripMenuItem.Click += new System.EventHandler(this.kopiujDoSchowkaToolStripMenuItem_Click);
			// 
			// kopiujZaznaczenieDoSchowkaToolStripMenuItem
			// 
			this.kopiujZaznaczenieDoSchowkaToolStripMenuItem.Name = "kopiujZaznaczenieDoSchowkaToolStripMenuItem";
			this.kopiujZaznaczenieDoSchowkaToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
			this.kopiujZaznaczenieDoSchowkaToolStripMenuItem.Text = "Kopiuj zaznaczenie do schowka";
			this.kopiujZaznaczenieDoSchowkaToolStripMenuItem.Click += new System.EventHandler(this.kopiujZaznaczenieDoSchowkaToolStripMenuItem_Click);
			// 
			// lbClientList
			// 
			this.lbClientList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lbClientList.ContextMenuStrip = this.cmsClientListBox;
			this.lbClientList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.lbClientList.FormattingEnabled = true;
			this.lbClientList.ItemHeight = 20;
			this.lbClientList.Location = new System.Drawing.Point(589, 117);
			this.lbClientList.Margin = new System.Windows.Forms.Padding(4, 5, 15, 15);
			this.lbClientList.Name = "lbClientList";
			this.lbClientList.Size = new System.Drawing.Size(171, 304);
			this.lbClientList.TabIndex = 7;
			// 
			// cmsClientListBox
			// 
			this.cmsClientListBox.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kopiujZaznaczenieToolStripMenuItem,
            this.wyrzucToolStripMenuItem,
            this.zbanujToolStripMenuItem});
			this.cmsClientListBox.Name = "cmsClientListBox";
			this.cmsClientListBox.Size = new System.Drawing.Size(181, 92);
			this.cmsClientListBox.Opening += new System.ComponentModel.CancelEventHandler(this.cmsClientListBox_Opening);
			// 
			// kopiujZaznaczenieToolStripMenuItem
			// 
			this.kopiujZaznaczenieToolStripMenuItem.Name = "kopiujZaznaczenieToolStripMenuItem";
			this.kopiujZaznaczenieToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.kopiujZaznaczenieToolStripMenuItem.Text = "Kopiuj zaznaczenie";
			this.kopiujZaznaczenieToolStripMenuItem.Click += new System.EventHandler(this.kopiujZaznaczenieDoSchowkaToolStripMenuItem_Click);
			// 
			// wyrzucToolStripMenuItem
			// 
			this.wyrzucToolStripMenuItem.Name = "wyrzucToolStripMenuItem";
			this.wyrzucToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.wyrzucToolStripMenuItem.Text = "Wyrzuc";
			this.wyrzucToolStripMenuItem.Click += new System.EventHandler(this.wyrzucToolStripMenuItem_Click);
			// 
			// zbanujToolStripMenuItem
			// 
			this.zbanujToolStripMenuItem.Name = "zbanujToolStripMenuItem";
			this.zbanujToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.zbanujToolStripMenuItem.Text = "Zbanuj";
			this.zbanujToolStripMenuItem.Click += new System.EventHandler(this.zbanujToolStripMenuItem_Click_1);
			// 
			// rtbMessage
			// 
			this.rtbMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.rtbMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.rtbMessage.Location = new System.Drawing.Point(24, 441);
			this.rtbMessage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 15);
			this.rtbMessage.Name = "rtbMessage";
			this.rtbMessage.Size = new System.Drawing.Size(736, 96);
			this.rtbMessage.TabIndex = 8;
			this.rtbMessage.Text = "";
			this.rtbMessage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rtbMessage_KeyDown);
			// 
			// nudPort
			// 
			this.nudPort.Location = new System.Drawing.Point(105, 75);
			this.nudPort.Margin = new System.Windows.Forms.Padding(15);
			this.nudPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
			this.nudPort.Name = "nudPort";
			this.nudPort.Size = new System.Drawing.Size(200, 26);
			this.nudPort.TabIndex = 9;
			this.nudPort.Value = new decimal(new int[] {
            1234,
            0,
            0,
            0});
			// 
			// fSerwer
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(784, 561);
			this.Controls.Add(this.nudPort);
			this.Controls.Add(this.rtbMessage);
			this.Controls.Add(this.lbClientList);
			this.Controls.Add(this.lbServerMessages);
			this.Controls.Add(this.tbAddress);
			this.Controls.Add(this.lPort);
			this.Controls.Add(this.lAddress);
			this.Controls.Add(this.bStop);
			this.Controls.Add(this.bStart);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.MinimumSize = new System.Drawing.Size(800, 600);
			this.Name = "fSerwer";
			this.Text = "Serwer";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.fServerTCP_FormClosed);
			this.cmsListBox.ResumeLayout(false);
			this.cmsClientListBox.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.nudPort)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bStart;
        private System.Windows.Forms.Button bStop;
        private System.Windows.Forms.Label lAddress;
        private System.Windows.Forms.Label lPort;
        private System.Windows.Forms.TextBox tbAddress;
        private System.Windows.Forms.ListBox lbServerMessages;
        private System.Windows.Forms.ListBox lbClientList;
        private System.Windows.Forms.RichTextBox rtbMessage;
        private System.Windows.Forms.NumericUpDown nudPort;
        private System.Windows.Forms.ContextMenuStrip cmsListBox;
        private System.Windows.Forms.ToolStripMenuItem wyczyśćToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zapiszLogiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kopiujDoSchowkaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kopiujZaznaczenieDoSchowkaToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmsClientListBox;
        private System.Windows.Forms.ToolStripMenuItem kopiujZaznaczenieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wyrzucToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zbanujToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveLogsDialog;
    }
}

