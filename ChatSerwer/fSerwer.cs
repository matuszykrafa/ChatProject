﻿using BibliotekaKlas;
using Newtonsoft.Json;
using SQLiteDatabase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ChatSerwer
{
	public partial class fSerwer : Form
	{
		private TcpListener server;
		private List<ClientClass> Clients = new List<ClientClass>();
		private List<string> clientList = new List<string>();

		private List<BanClass> bans = new List<BanClass>();
		private List<JsonMessage> oldMsgs = new List<JsonMessage>();
		private List<JsonClient> registredUsers = new List<JsonClient>();

		private bool activeServer = false;
		private Thread serverThread;
		public fSerwer()
		{
			InitializeComponent();
			lbServerMessages.HorizontalScrollbar = true;
			bStop.Enabled = false;
		}


		#region Wątek serwera
		private void serverWork()
		{

			IPAddress adresIP = null;
			int port = System.Convert.ToUInt16(nudPort.Value);

			try {
				adresIP = IPAddress.Parse(tbAddress.Text);
			}
			catch {
				MessageBox.Show("Błędny format adresu IP!", "Błąd");
				tbAddress.Invoke(new MethodInvoker(delegate { tbAddress.Text = "0.0.0.0"; }));

				return;
			}

			try {
				server = new TcpListener(adresIP, port);

				server.Start();

				while (true) {

					TcpClient client = server.AcceptTcpClient();

					IPEndPoint IP = (IPEndPoint)client.Client.RemoteEndPoint;

					lbServerMessages.Invoke(new MethodInvoker(delegate {
						lbServerMessages.Items.Add(currentTime() + " [" + IP.ToString() + "] : Nawiązano połączenie");
						lbServerMessages.TopIndex = lbServerMessages.Items.Count - 1;
					}));



					Thread newClient = new Thread(ClientThread);
					newClient.Start(client);
				}
			}
			catch (FormatException) {
				MessageBox.Show("Format Exception", "Błąd");
				activeServer = false;
			}
			catch (SocketException) {
				//MessageBox.Show("Socket Exception", "Błąd");
				activeServer = false;
			}
			finally {
				activeServer = false;
				server.Stop();
			}
		}
		#endregion

		#region Wątek klienta
		private void ClientThread(object newClient)
		{
			ClientClass curClient = new ClientClass();
			Clients.Add(curClient);
			curClient.Connection(newClient);

			try {
				while (true) {
					try {
						string received = curClient.reader.ReadLine();

						var receivedJson = JsonConvert.DeserializeObject<JsonMessage>(received);
						if (receivedJson.wiadomosc_typ == "register") {
							JsonClient userRegister = JsonConvert.DeserializeObject<JsonClient>(receivedJson.wiadomosc_dane);
							registredUsers = UserDatabase.LoadUsers();
							bool register = true;
							foreach (JsonClient jsonClient in registredUsers) {
								if (jsonClient.user == userRegister.user || jsonClient.email == userRegister.email) {
									register = false;
								}
							}
							if (register) {
								UserDatabase.RegisterUser(userRegister);
								lbServerMessages.Invoke(new MethodInvoker(delegate {
									lbServerMessages.Items.Add(currentTime() + " [" + curClient.IP.ToString() + "] [" + userRegister.user + "] : Zarejestrowano.");
								}));
								send("allowRegister", curClient, "Server", "server", currentTimeSend());
							}
							else {
								lbServerMessages.Invoke(new MethodInvoker(delegate {
									lbServerMessages.Items.Add(currentTime() + " [" + curClient.IP.ToString() + "] [" + userRegister.user + "] : Próba ponownej rejestracji.");
								}));
								send("denyRegister", curClient, "Server", "server", currentTimeSend());
							}
						}
						else if (receivedJson.wiadomosc_typ == "login") {
							JsonClient userLogin = JsonConvert.DeserializeObject<JsonClient>(receivedJson.wiadomosc_dane);
							registredUsers = UserDatabase.LoadUsers();
							bool login = false;

							foreach (JsonClient jsonClient in registredUsers) {
								var hash = new Rfc2898DeriveBytes(userLogin.password, Convert.FromBase64String(jsonClient.salt), 10000).GetBytes(32);
								//Console.WriteLine(Convert.ToBase64String(hash));
								//Console.WriteLine(jsonClient.password);
								//Console.WriteLine(jsonClient.user + " " + userLogin.user);
								if (jsonClient.user == userLogin.user && jsonClient.password == Convert.ToBase64String(hash)) {
									login = true;
								}
							}

							if (login) {
								bool logged = false;
								foreach (string client in clientList) {
									if (client == userLogin.user) {
										logged = true;
									}
								}
								if (!logged) {
									send("allowLogin", curClient, "Server", "server", currentTimeSend());
									curClient.nick = userLogin.user;
									refreshClientList();
									lbServerMessages.Invoke(new MethodInvoker(delegate {
										lbServerMessages.Items.Add(currentTime() + " [" + curClient.IP.ToString() + "] [" + userLogin.user + "] : Zalogowano.");
									}));
								}
								else {
									send("denyLoginLogged", curClient, "Server", "server", currentTimeSend());
									lbServerMessages.Invoke(new MethodInvoker(delegate {
										lbServerMessages.Items.Add(currentTime() + " [" + curClient.IP.ToString() + "] [" + userLogin.user + "] : Nieudana próba logowania. Użytkownik jest już zalogowany.");
									}));
								}

							}
							else {
								lbServerMessages.Invoke(new MethodInvoker(delegate {
									lbServerMessages.Items.Add(currentTime() + " [" + curClient.IP.ToString() + "] [" + userLogin.user + "] : Nieudana próba logowania. Niepoprawny login i/lub hasło.");
								}));

								send("denyLogin", curClient, "Server", "server", currentTimeSend());
							}
						}
						else if (receivedJson.wiadomosc_typ == "init") {
							//lbServerMessages.Invoke(new MethodInvoker(delegate {
							//	lbServerMessages.Items.Add(currentTime() + " [" + curClient.IP.ToString() + "] [" + receivedJson.nadawca + "] : Zalogowano.");
							//}));

							//curClient.nick = receivedJson.nadawca;
							//refreshClientList();
							//send("add", curClient, "Server", "server", currentTimeSend());

							bool add = true;
							bool banned = false;
							foreach (ClientClass client in Clients) {
								if (client.nick == receivedJson.nadawca) {
									add = false;
								}
							}
							bans = BansDatabase.LoadBan();
							foreach (BanClass ban in bans) {
								if (ban.user == receivedJson.nadawca || ban.ip == curClient.IP.ToString()) {
									banned = true;
									break;
								}
							}
							if (add && !banned) {
								lbServerMessages.Invoke(new MethodInvoker(delegate {
									lbServerMessages.Items.Add(currentTime() + " [" + curClient.IP.ToString() + "] [" + receivedJson.nadawca + "] : Zalogowano.");
								}));
								curClient.nick = receivedJson.nadawca;
								refreshClientList();
								send("add", curClient, "Server", "server", currentTimeSend());
							}
							else if (!add && !banned) {
								send("logged", curClient, "Server", "server", currentTimeSend());
								break;
							}
							else if (add && banned) {
								send("banned", curClient, "Server", "server", currentTimeSend());
								break;
							}
						}
						else if (receivedJson.wiadomosc_typ == "setReceiver") {
							lbServerMessages.Invoke(new MethodInvoker(delegate {
								lbServerMessages.Items.Add(currentTime() + " [" + curClient.IP.ToString() + "] [" + receivedJson.nadawca + "] : Pobieranie konwersacji z: [" + receivedJson.wiadomosc_dane + "].");
							}));
							oldMsgs = MessagesDatabase.LoadMessage();
							if (oldMsgs.Count != 0) {
								JsonMessage last = oldMsgs.Last();
								foreach (JsonMessage msg in oldMsgs) {
									if (msg.adresat == receivedJson.nadawca && msg.nadawca == receivedJson.wiadomosc_dane) {
										if (msg.Equals(last)) {
											send(msg.wiadomosc_dane + "<script>document.body.scrollTop = document.body.scrollHeight</script>", curClient, msg.nadawca, "textZwrotReceived", msg.czas_wyslania);
										}
										else {
											send(msg.wiadomosc_dane, curClient, msg.nadawca, "textZwrotReceived", msg.czas_wyslania);
										}
									}
									else if (msg.adresat == receivedJson.wiadomosc_dane && msg.nadawca == receivedJson.nadawca) {
										if (msg.Equals(last)) {
											send(msg.wiadomosc_dane + "<script>document.body.scrollTop = document.body.scrollHeight</script>", curClient, msg.nadawca, "textZwrotSent", msg.czas_wyslania);
										}
										else {
											send(msg.wiadomosc_dane, curClient, msg.nadawca, "textZwrotSent", msg.czas_wyslania);
										}

									}
								}
								send("koniec pobierania", curClient, receivedJson.wiadomosc_dane, "textZwrotKoniec", currentTimeSend());
							}

						}
						else if (receivedJson.wiadomosc_typ == "text") {
							if (receivedJson.wiadomosc_dane != null) {
								curClient.nick = receivedJson.nadawca;
								MessagesDatabase.SaveMessage(receivedJson);

								foreach (ClientClass client in Clients) {
									if (client.nick == receivedJson.adresat) {
										send(receivedJson.wiadomosc_dane, client, receivedJson.nadawca, receivedJson.wiadomosc_typ, receivedJson.czas_wyslania);


									}
								}
							}
						}
						else
							break;
					}
					catch {
						break;
					}
				};

				if (curClient.writer.BaseStream != null) curClient.writer.Close();
				if (curClient.reader.BaseStream != null) curClient.reader.Close();

				Clients.Remove(curClient);
				refreshClientList(); //HALO MUTEX
				curClient.client.Close();
			}
			catch (Exception ex) {
				//lbServerMessages.Invoke(new MethodInvoker(delegate {
				//    lbServerMessages.Items.Add(currentTime() + " : Problem z połączeniem.");
				//    lbServerMessages.TopIndex = lbServerMessages.Items.Count - 1;
				//}));

			}
			finally {
				if (curClient != null) {
					Clients.Remove(curClient);
					refreshClientList(); //TU BŁĄD MUTEX
					if (activeServer) {
						lbServerMessages.Invoke(new MethodInvoker(delegate {
							if (curClient.nick != null) {
								lbServerMessages.Items.Add(currentTime() + " [" + curClient.IP.ToString() + "] [" + curClient.nick + "] : Zakończono połączenie.");
							}
							else {
								lbServerMessages.Items.Add(currentTime() + " [" + curClient.IP.ToString() + "] : Zakończono połączenie.");
							}

						}));
						curClient.client.Close();
					}

				}
			}
		}

		#endregion

		#region Lista dostępnych klientów
		private void refreshClientList()
		{
			if (activeServer) {
				lbClientList.Invoke(new MethodInvoker(delegate {
					lbClientList.Items.Clear();
					clientList.Clear();
					foreach (ClientClass client in Clients) {
						if (client.nick != null) {
							lbClientList.Items.Add(client.nick);
							clientList.Add(client.nick);
						}


					}
					foreach (ClientClass client in Clients) {
						string lista = string.Join(";", clientList);
						if (client.nick != null)
							send(lista, client, "Server", "clientList", currentTimeSend());
					}
					lbClientList.Refresh();
				}));
			}
		}
		#endregion

		#region Wiadomości serwera (enter)
		private void rtbMessage_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter) {
				e.Handled = true;
				string msg = rtbMessage.Text;
				if (msg == null) {

				}
				else if (msg[0].ToString() == "/") {
					string[] komenda = msg.Split(' ');
					if (komenda[0] == "/msg") {
						if (komenda.Length > 2) {
							string wiadomosc = "";
							for (int i = 2; i < komenda.Length; i++) {
								wiadomosc = wiadomosc + komenda[i] + " ";
							}
							foreach (ClientClass client in Clients) {
								if (client.nick == komenda[1]) {
									send(wiadomosc, client, "Server", "text", currentTimeSend());
									break;
								}
							}
						}
					}
					else if (komenda[0] == "/msgall") {
						string wiadomosc = "";
						for (int i = 1; i < komenda.Length; i++) {
							wiadomosc = wiadomosc + komenda[i] + " ";
						}
						foreach (ClientClass client in Clients) {
							send(wiadomosc, client, "Server", "text", currentTimeSend());
						}
					}
					else if (komenda[0] == "/kick") {
						string nick = "";
						for (int i = 1; i < komenda.Length; i++) {
							if (i != 1)
								nick += " ";
							nick = nick + komenda[i];
						}
						wyrzuc(nick);
					}
					else if (komenda[0] == "/kickIP") {
						string ip = "";
						for (int i = 1; i < komenda.Length; i++) {
							if (i != 1)
								ip += " ";
							ip = ip + komenda[i];
						}
						wyrzucIP(ip);
					}
					else if (komenda[0] == "/ban") {
						string nick = "";
						for (int i = 1; i < komenda.Length; i++) {
							if (i != 1)
								nick += " ";
							nick = nick + komenda[i];
						}
						banuj(nick);
					}
					else if (komenda[0] == "/help" || komenda[0] == "/?") {
						lbServerMessages.Invoke(new MethodInvoker(delegate {
							lbServerMessages.Items.Add("=== HELP ===");
							lbServerMessages.Items.Add("/msg [nick] [wiadomosc] - wiadomość do wybranego użytkownika");
							lbServerMessages.Items.Add("/msgall [wiadomosc] - wiadomość do wszystykich użytkowników");
							lbServerMessages.Items.Add("/kick [nick] - wyrzucenie danego użytkownika");
							lbServerMessages.Items.Add("/kickIP [IP] - wyrzucenie danego użytkownika po adresie IP");
							lbServerMessages.Items.Add("/ban [nick] - zbanowanie danego użytkownika");
							lbServerMessages.Items.Add("/help OR /? - wyświetlenie tego komunikatu");
							lbServerMessages.TopIndex = lbServerMessages.Items.Count - 1;
						}));
					}
					else {
						lbServerMessages.Invoke(new MethodInvoker(delegate {
							lbServerMessages.Items.Add(msg + ": nierozpoznano polecenia. Wpisz /help albo /?.");
							lbServerMessages.TopIndex = lbServerMessages.Items.Count - 1;
						}));
					}
				}
				else {
					lbServerMessages.Invoke(new MethodInvoker(delegate {
						lbServerMessages.Items.Add(msg + ": nierozpoznano polecenia. Wpisz /help albo /?.");
						lbServerMessages.TopIndex = lbServerMessages.Items.Count - 1;
					}));
				}
				rtbMessage.Clear();
			}
		}

		#endregion

		#region Wiadomości serwera (send, sendPrepare)
		private string sendPrepare(string msg, string adresat, string nadawca, string typ, string czas_wyslania)
		{
			JsonMessage wiadomoscObiekt = new JsonMessage();
			wiadomoscObiekt.nadawca = nadawca;
			wiadomoscObiekt.adresat = adresat;
			wiadomoscObiekt.czas_wyslania = czas_wyslania;
			wiadomoscObiekt.wiadomosc_typ = typ;
			wiadomoscObiekt.wiadomosc_dane = msg;
			string output = JsonConvert.SerializeObject(wiadomoscObiekt);
			return output;
		}

		private void send(string msg, ClientClass curClient, string nadawca, string typ, string czas_wyslania)
		{
			try {
				if (curClient.writer != null && curClient.writer.BaseStream != null) {
					curClient.writer.WriteLine(sendPrepare(msg, curClient.nick, nadawca, typ, czas_wyslania));
					curClient.writer.Flush();
					lbServerMessages.Invoke(new MethodInvoker(delegate {
						if (typ == "text")
							lbServerMessages.Items.Add(currentTime() + " [" + nadawca + " -> " + curClient.nick + "] : " + msg);
						else if (typ == "server")
							lbServerMessages.Items.Add(currentTime() + " [" + nadawca + " -> " + curClient.IP + "] : " + msg);
						lbServerMessages.TopIndex = lbServerMessages.Items.Count - 1;
					}));

				}
			}
			catch {
				Console.WriteLine("BŁĄD PRZY WYSŁYANIU");
			}

		}

		#endregion

		#region Przyciski Start, Stop
		private void bStart_Click(object sender, EventArgs e)
		{
			lbServerMessages.Items.Add(currentTime() + " : Serwer włączony.");
			lbServerMessages.TopIndex = lbServerMessages.Items.Count - 1;
			serverThread = new Thread(serverWork);
			serverThread.Start();
			bStart.Enabled = false;
			bStop.Enabled = true;
			activeServer = true;
			//wbChat.DocumentText = "<html><head> <meta charset='UTF-8'> <link rel='stylesheet' href='https://jsnm.ga/rafalSerwerTCP.css'></head><body>";
		}

		private void bStop_Click(object sender, EventArgs e)
		{
			activeServer = false;
			lbServerMessages.Items.Add(currentTime() + " : Zakończono pracę serwera.");
			stop();

			lbClientList.Items.Clear();
			clientList.Clear();

			lbServerMessages.TopIndex = lbServerMessages.Items.Count - 1;
			bStart.Enabled = true;
			bStop.Enabled = false;
		}
		#endregion

		#region Metody stop, zamykanie programu

		private void fServerTCP_FormClosed(object sender, FormClosedEventArgs e)
		{
			stop();
		}

		private void stop()
		{
			if (Clients != null)
				foreach (ClientClass client in Clients) {
					if (client.reader != null && client.reader.BaseStream != null)
						client.reader.Close();
					if (client.writer != null && client.writer.BaseStream != null)
						client.writer.Close();
				}

			if (server != null)
				server.Stop();

			if (Clients != null)
				foreach (ClientClass client in Clients) {
					if (client.client != null)
						client.client.Close();
				}

			Clients.Clear();
			if (activeServer)
				refreshClientList(); //MUTEX
		}
		#endregion

		#region Kick, ban

		private void wyrzuc(string nick)
		{
			bool wyrzucony = false;
			foreach (ClientClass client in Clients) {
				for (int i = 0; i < Clients.Count; i++) {
					if (client.nick == nick) {

						if (client.writer.BaseStream != null) client.writer.Close();
						if (client.reader.BaseStream != null) client.reader.Close();

						lbServerMessages.Items.Add(currentTime() + " [" + client.IP + "] " + "[" + client.nick + "] : Został wyrzucony.");
						client.client.Close();
						Clients.Remove(client);
						refreshClientList();

						wyrzucony = true;
						break;
					}
				}
				if (wyrzucony) {
					break;
				}
			}
		}

		private void wyrzucIP(string ip)
		{
			bool wyrzucony = false;
			foreach (ClientClass client in Clients) {
				for (int i = 0; i < Clients.Count; i++) {
					if (client.IP.ToString() == ip) {

						if (client.writer.BaseStream != null) client.writer.Close();
						if (client.reader.BaseStream != null) client.reader.Close();

						lbServerMessages.Items.Add(currentTime() + " [" + client.IP + "] : Został wyrzucony.");
						client.client.Close();
						Clients.Remove(client);
						refreshClientList();

						wyrzucony = true;
						break;
					}
				}
				if (wyrzucony) {
					break;
				}
			}
		}

		private void banuj(string nick)
		{
			BanClass ban = new BanClass();
			ban.user = nick;
			foreach (ClientClass client in Clients) {
				if (ban.user == client.nick) {
					ban.ip = client.IP.ToString();
					lbServerMessages.Items.Add(currentTime() + " [" + client.IP + "] " + "[" + client.nick + "] : Został zbanowany.");
					client.client.Close();
					Clients.Remove(client);
					refreshClientList();
					break;
				}
			}
			BansDatabase.SaveBan(ban);
			//bool zbanowany = false;
			//foreach (ClientClass client in Clients) {
			//    for (int i = 0; i < Clients.Count; i++) {
			//        if (client.nick.ToString() == nick) {
			//            if (client.writer.BaseStream != null) client.writer.Close();
			//            if (client.reader.BaseStream != null) client.reader.Close();

			//            lbServerMessages.Items.Add(currentTime() + " [" + client.IP + "] " + "[" + client.nick + "] : Został zbanowany.");
			//            bans.Add(client.nick);
			//            bansIP.Add(client.IP.ToString());
			//            client.client.Close();
			//            Clients.Remove(client);
			//            refreshClientList();

			//            zbanowany = true;
			//            break;
			//        }
			//    }
			//    if (zbanowany) {
			//        break;
			//    }
			//}
		}

		#endregion

		#region cmsListBox
		private void cmsListBox_Opening(object sender, CancelEventArgs e)
		{
			if (lbServerMessages.SelectedItem == null)
				cmsListBox.Items[3].Enabled = false;
			else
				cmsListBox.Items[3].Enabled = true;
		}

		private void wyczyśćToolStripMenuItem_Click(object sender, EventArgs e)
		{
			lbServerMessages.Items.Clear();
		}

		private void zapiszLogiToolStripMenuItem_Click(object sender, EventArgs e)
		{
			saveLogsDialog.DefaultExt = "txt";
			saveLogsDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
			saveLogsDialog.FileName = "logs";
			saveLogsDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			if (saveLogsDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK) {

				string logs = "";
				for (int i = 0; i < lbServerMessages.Items.Count; i++) {
					logs += lbServerMessages.Items[i].ToString() + "\n";
				}
				File.WriteAllText(saveLogsDialog.FileName, logs);
			}
		}

		private void kopiujDoSchowkaToolStripMenuItem_Click(object sender, EventArgs e)
		{
			string logs = "";
			for (int i = 0; i < lbServerMessages.Items.Count; i++) {
				logs += lbServerMessages.Items[i].ToString() + "\n";
			}
			if (logs != "") {
				System.Windows.Forms.Clipboard.SetText(logs);
			}
		}

		private void kopiujZaznaczenieDoSchowkaToolStripMenuItem_Click(object sender, EventArgs e)
		{
			System.Windows.Forms.Clipboard.SetText(lbServerMessages.SelectedItem.ToString());
		}

		#endregion

		#region cmsClientListBox
		private void cmsClientListBox_Opening(object sender, CancelEventArgs e)
		{
			if (lbClientList.SelectedItem == null) {
				cmsClientListBox.Items[0].Enabled = false;
				cmsClientListBox.Items[1].Enabled = false;
				cmsClientListBox.Items[2].Enabled = false;
			}
			else {
				cmsClientListBox.Items[0].Enabled = true;
				cmsClientListBox.Items[1].Enabled = true;
				cmsClientListBox.Items[2].Enabled = true;
			}
		}
		private void wyrzucToolStripMenuItem_Click(object sender, EventArgs e)
		{
			try {
				wyrzuc(lbClientList.SelectedItem.ToString());
			}
			catch {

			}
		}

		private void zbanujToolStripMenuItem_Click_1(object sender, EventArgs e)
		{
			banuj(lbClientList.SelectedItem.ToString());
		}

		#endregion

		#region czas
		private string currentTime()
		{
			int ihour = DateTime.Now.Hour;
			string hour;
			if (ihour < 10)
				hour = "0" + ihour.ToString();
			else
				hour = ihour.ToString();

			int imin = DateTime.Now.Minute;
			string min;
			if (imin < 10)
				min = "0" + imin.ToString();
			else
				min = imin.ToString();

			int isec = DateTime.Now.Second;
			string sec;
			if (isec < 10)
				sec = "0" + isec.ToString();
			else
				sec = isec.ToString();

			string curTime = "[" + hour + ":" + min + ":" + sec + "]";
			return curTime;
		}

		private string currentTimeSend()
		{
			int ihour = DateTime.Now.Hour;
			string hour;
			if (ihour < 10)
				hour = "0" + ihour.ToString();
			else
				hour = ihour.ToString();

			int imin = DateTime.Now.Minute;
			string min;
			if (imin < 10)
				min = "0" + imin.ToString();
			else
				min = imin.ToString();

			string curTime = hour + ":" + min;

			return curTime;
		}
		#endregion
	}
}
